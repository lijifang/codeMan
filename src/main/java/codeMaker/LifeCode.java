package codeMaker;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;

import constant.ChildWindowConstant;
import constant.Constant;

public class LifeCode {

	private JFrame frame;
	private JTextField gzhCode;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				File file = new File(Constant.lifePropertiesPath);
				if (!file.exists()) {
					file.createNewFile();
				}

				Properties properties = new Properties();
				BufferedReader reader;
				reader = new BufferedReader(new FileReader(new File(Constant.lifePropertiesPath)));
				properties.load(reader);
				String status = properties.getProperty("status");
				reader.close();

				if ("1".equals(status)) {
					MakeCode.main(new String[]{});
					return;
				}
				//使用 java 自带皮肤
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
				LifeCode lifeCode = new LifeCode();
				JFrame.setDefaultLookAndFeelDecorated(true);
				JDialog.setDefaultLookAndFeelDecorated(true);
				lifeCode.frame.setVisible(true);

			} catch (Exception ignored) {
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LifeCode() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setTitle("激活窗口");
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(LifeCode.class.getResource(
				"/org/pushingpixels/substance/internal/contrib/randelshofer/quaqua/images/color_wheel.png")));
		frame.setBounds(100, 100, 492, 296);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel("请输入激活码：");

		gzhCode = new JTextField();
		gzhCode.setColumns(10);

		JButton btnNewButton = new JButton("扫一扫获取");
		btnNewButton.addActionListener(e -> {

			// 去往公众号扫码
			if (ChildWindowConstant.vxGzhCode == null) {
				VxGzhCode.main(new String[]{});
				return;
			}
			ChildWindowConstant.vxGzhCode.dispose();
			ChildWindowConstant.vxGzhCode = null;
			VxGzhCode.main(new String[]{});

		});

		JButton button = new JButton("激活");
		button.addActionListener(e -> {

			List<String> codeList = new ArrayList<>();

			codeList.add("qaz135");
			codeList.add("awk667");
			codeList.add("qwe239");
			codeList.add("1wrtt");
			codeList.add("7ui90o");

			String lifeText = gzhCode.getText();

			if (codeList.contains(lifeText)) {

				BufferedReader bufferedReader = null;
				FileOutputStream outputStream = null;
				Properties properties = new Properties();
				try {
					bufferedReader = new BufferedReader(new FileReader(Constant.lifePropertiesPath));
					properties.load(bufferedReader);
					outputStream = new FileOutputStream(new File(Constant.lifePropertiesPath));
					properties.setProperty("status", "1");
					properties.store(outputStream, "status");
				} catch (Exception ignored) {
				} finally {
					if (bufferedReader != null) {
						try {
							bufferedReader.close();
						} catch (IOException ignored) {
						}
					}
					if (outputStream != null) {
						try {
							outputStream.close();
						} catch (IOException ignored) {
						}
					}
				}

				JOptionPane.showMessageDialog(frame, "恭喜，激活成功！", "提示", JOptionPane.INFORMATION_MESSAGE);

				frame.dispose();

				MakeCode.main(new String[]{});

				return;

			}

			JOptionPane.showMessageDialog(frame, "激活码输入有误，激活失败！", "错误", JOptionPane.ERROR_MESSAGE);

		});

		JLabel lbljavapython = new JLabel(
				"<html><body>关注微信公众号 螺旋编程极客 免费获取激活码！<br/>更有海量编程教学：如java，python，大数据，小程序等教程资料领取！</body></html>");
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
						.createSequentialGroup().addGap(83)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(gzhCode, GroupLayout.PREFERRED_SIZE, 127,
												GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(btnNewButton))
								.addComponent(label).addComponent(lbljavapython)))
						.addGroup(groupLayout.createSequentialGroup().addGap(114).addComponent(button,
								GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(35, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(40).addComponent(label)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(gzhCode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton))
						.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(button).addGap(30)
						.addComponent(lbljavapython).addContainerGap(60, Short.MAX_VALUE)));
		frame.getContentPane().setLayout(groupLayout);
	}
}
