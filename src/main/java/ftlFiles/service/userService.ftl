package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.dao.LoginDao;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.User;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

	private final LoginDao dao;

	@Autowired
	public LoginServiceImpl(LoginDao dao) {
		this.dao = dao;
	}

	@Override
	public User login(User user) {
		return dao.login(user);
	}

}
