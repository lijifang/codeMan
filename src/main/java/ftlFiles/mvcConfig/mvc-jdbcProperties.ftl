<#if isMutiDataSource>
<#list dataSourceModelMap?keys as key>
<#assign sourceModel = dataSourceModelMap["${key}"]/>
${key}.jdbc.driver=${sourceModel.dataBaseDriverClass!""}
${key}.jdbc.url=${sourceModel.dataBaseUrl!""}
${key}.jdbc.username=${sourceModel.dataBaseUserNameVal!""}
${key}.jdbc.password=${sourceModel.dataBasePwdVal!""}
<#if databasePool == "Druid">
${key}.jdbc.filters=stat
${key}.jdbc.maxActive=20
${key}.jdbc.initialSize=1
${key}.jdbc.maxWait=60000
${key}.jdbc.minIdle=10
${key}.jdbc.timeBetweenEvictionRunsMillis=60000
${key}.jdbc.minEvictableIdleTimeMillis=300000
<#if sourceModel.dataBaseTypeVal == "mysql" || sourceModel.dataBaseTypeVal == "postgresql" || sourceModel.dataBaseTypeVal == "sqlserver">
${key}.jdbc.validationQuery=SELECT 1
</#if>
<#if sourceModel.dataBaseTypeVal == "oracle">
${key}.jdbc.validationQuery=SELECT 1 FROM DUAL
</#if>
${key}.jdbc.testWhileIdle=true
${key}.jdbc.testOnBorrow=false
${key}.jdbc.testOnReturn=false
${key}.jdbc.maxOpenPreparedStatements=20
${key}.jdbc.removeAbandoned=true
${key}.jdbc.removeAbandonedTimeout=1800
${key}.jdbc.logAbandoned=true

</#if>
<#if databasePool == "HikariCP">
${key}.jdbc.minimum-idle=5
${key}.jdbc.idle-timeout=30000
${key}.jdbc.maximum-pool-size=10
${key}.jdbc.auto-commit=true
${key}.jdbc.max-lifetime=120000
${key}.jdbc.connection-timeout=30000
<#if sourceModel.dataBaseTypeVal == "mysql" || sourceModel.dataBaseTypeVal == "postgresql" || sourceModel.dataBaseTypeVal == "sqlserver">
${key}.jdbc.connection-test-query=SELECT 1

</#if>
<#if sourceModel.dataBaseTypeVal == "oracle">
${key}.jdbc.connection-test-query=SELECT 1 FROM DUAL

</#if>
</#if>
</#list>
<#else>
jdbc.driver=${dataBaseDriverClass!""}
jdbc.url=${dataBaseUrl!""}
jdbc.username=${dataBaseUserName!""}
jdbc.password=${dataBasePwd!""}

<#if databasePool == "Druid">
jdbc.filters=stat
jdbc.maxActive=20
jdbc.initialSize=1
jdbc.maxWait=60000
jdbc.minIdle=10
jdbc.timeBetweenEvictionRunsMillis=60000
jdbc.minEvictableIdleTimeMillis=300000
<#if dataBaseType == "mysql" || dataBaseType == "postgresql" || dataBaseType == "sqlserver">
jdbc.validationQuery=SELECT 1
</#if>
<#if dataBaseType == "oracle">
jdbc.validationQuery=SELECT 1 FROM DUAL
</#if>
jdbc.testWhileIdle=true
jdbc.testOnBorrow=false
jdbc.testOnReturn=false
jdbc.maxOpenPreparedStatements=20
jdbc.removeAbandoned=true
jdbc.removeAbandonedTimeout=1800
jdbc.logAbandoned=true
</#if>

<#if databasePool == "HikariCP">
jdbc.minimum-idle=5
jdbc.idle-timeout=30000
jdbc.maximum-pool-size=10
jdbc.auto-commit=true
jdbc.max-lifetime=1200000
jdbc.connection-timeout=30000
<#if dataBaseType == "mysql" || dataBaseType == "postgresql" || dataBaseType == "sqlserver">
jdbc.connection-test-query=SELECT 1
</#if>
<#if dataBaseType == "oracle">
jdbc.connection-test-query=SELECT 1 FROM DUAL
</#if>
</#if>
</#if>


