server:
  port: 7001
<#if cloudRegisteCenter=="eureka">
eureka:
  client:
    fetch-registry: true
    register-with-eureka: true
    service-url:
      # eureka地址
      defaultZone: http://127.0.0.1:8001/eureka/
</#if>
spring:
  application:
    name: sys-gateway
  cloud:
<#if cloudRegisteCenter=="nacos">
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
        heart-beat-interval: 30 #30S发送一次心跳
        heart-beat-timeout: 90 #90S 如果没发送心跳成功，就自动下线
        weight: 1 #设置实例权重，权重越高，处理服务能力越强
        group: ${cloudSysEngName} #微服务分组
</#if>
    gateway:
      discovery:
        locator:
          enabled: true #开启从注册中心定位路由服务
      httpclient:
        connect-timeout: 30000
        response-timeout: 30s
      globalcors:
        cors-configurations:
          '[/**]':
            # 允许携带认证信息
            allow-credentials: true
            # 允许跨域的源(网站域名/ip)，设置*为全部
            allowedOrigins: "*"
            # 允许跨域的method， 默认为GET和OPTIONS，设置*为全部
            allowedMethods: "*"
            # 允许跨域请求里的head字段，设置*为全部
            allowedHeaders: "*"
            max-age: 3600
      routes:
        - id: system-service # 路由标示，必须唯一
          uri: lb://system-service # 路由的目标地址
          filters:
            #- PrefixPath=/system-service
             - StripPrefix=1
          predicates: # 路由断言，判断请求是否符合规则
            - Path=/system-service/** # 路径断言，判断路径是否是以/user开头，如果是则符合
<#list cloudServices as service>
        - id: ${service}-service
          uri: lb://${service}-service
          filters:
            # 1表示过滤一个路径
             - StripPrefix=1
          predicates:
            - Path=/${service}-service/**
</#list>
info:
  app.name: 网关gateway
  company.name: codeMan出品
  build.artifactId: @project.artifactId@
  build.version: @project.version@
